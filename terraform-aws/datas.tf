data "template_file" "data_userdata_script" {
  template = "${file("${path.module}/../templates/user_data.sh")}"

  vars {
    cloud_provider = "aws"
    #access_key = "ASIAIFPMD3GPEZVF6Z3Q"
    #secret_key = "L86y/n6IF2ZolNpfIkf/HrZy+fRae5amdm3Q07l0"
    #token = "FQoDYXdzENT//////////wEaDEdyvZchux+biuyNryLOAhUSvjx+BJVKjahL5u8lDC3UM4Wg10mrrX35Z8nXBaHppJDXkl/BGY74PyPNqIDKKzJ+JOkl75Q/nyHqWEINqoRtaIDyQInKMIHbeV8SSRsfeXgeT6geQv1+5xnbRPUVtLPgr6MPQCl3PrexC1BOF2WNje0LYgACNL/wGo6kZy67eD4zAaS1DaDumE+CoLJZ7E9Oc/UiTYntlboIlVIpggdsRYjKgokYRTWPVxfEmil7i0Y8Ni4aqU6/SzAxLh0eh7sgXJg4Fg2cIStW/itsxsS2eHIn4o4x6tdQhqJevSnF+VRdDCQjeSRmDHMAPm6aTn2YjP8hh+Aa+G9EXs11CR9EScRHt8nHc/OeylQA3W10V/BzTsWAuJPM8Z12ekCovTjTVNCpB55X2FL4Ut539pPJ8ZMHHiC9MucoOVX+MDd/axUkO8ZmsKD2i5tRlhIoiN7w0wU="
    volume_name             = "${var.volume_name}"
    elasticsearch_data_dir  = "${var.elasticsearch_data_dir}"
    elasticsearch_logs_dir  = "${var.elasticsearch_logs_dir}"
    heap_size               = "${var.data_heap_size}"
    es_cluster              = "${var.es_cluster}"
    es_environment          = "${var.environment}-${var.es_cluster}"
    security_groups         = "${aws_security_group.elasticsearch_security_group.id}"
    aws_region              = "${var.aws_region}"
    availability_zones      = "${join(",", coalescelist(var.availability_zones, data.aws_availability_zones.available.names))}"
    minimum_master_nodes    = "${format("%d", var.masters_count / 2 + 1)}"
    master                  = "false"
    data                    = "true"
    http_enabled            = "true"
    security_enabled        = "${var.security_enabled}"
    monitoring_enabled      = "${var.monitoring_enabled}"
    client_user             = ""
    client_pwd              = ""
  }
}

resource "aws_launch_configuration" "data" {
  name_prefix = "elasticsearch-${var.es_cluster}-data-nodes"
  image_id = "${data.aws_ami.elasticsearch.id}"
  instance_type = "${var.data_instance_type}"
  security_groups = ["${aws_security_group.elasticsearch_security_group.id}"]
  associate_public_ip_address = false
  iam_instance_profile = "${aws_iam_instance_profile.elasticsearch.id}"
  user_data = "${data.template_file.data_userdata_script.rendered}"
  key_name = "${var.key_name}"

  ebs_optimized = true

  lifecycle {
    create_before_destroy = true
  }

  ebs_block_device {
    device_name = "${var.volume_name}"
    volume_size = "${var.elasticsearch_volume_size}"
    encrypted = "${var.volume_encryption}"
  }
}

resource "aws_autoscaling_group" "data_nodes" {
  name = "elasticsearch-${var.es_cluster}-data-nodes"
  max_size = "${var.datas_count}"
  min_size = "${var.datas_count}"
  desired_capacity = "${var.datas_count}"
  default_cooldown = 30
  force_delete = true
  launch_configuration = "${aws_launch_configuration.data.id}"

  vpc_zone_identifier = ["${data.aws_subnet_ids.selected.ids}"]

  depends_on = ["aws_autoscaling_group.master_nodes"]

  tag {
    key                 = "Name"
    value               = "${format("%s-data-node", var.es_cluster)}"
    propagate_at_launch = true
  }

  tag {
    key = "Environment"
    value = "${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key = "Cluster"
    value = "${var.environment}-${var.es_cluster}"
    propagate_at_launch = true
  }

  tag {
    key = "Role"
    value = "data"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}